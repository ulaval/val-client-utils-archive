"""
Ce module permet de télécharger des fichiers à partir de la partition download de slurm
"""
from plumbum import local, TEE
import time


def wget(urls, partition=None):
    part = partition if partition else "download"
    salloc = local["salloc"]
    ls = local["ls"]
    for url in urls:
        print(f"Initializing download for {url}")

        f = salloc[
                f"--partition={part}",
                "--cpus-per-task=1",
                "--mem=256M",
                "srun",
                "/usr/bin/wget",
                "--progress=dot",
                "-e",
                "dotbytes=5M",
                url] & TEE
        print(f"COMPLETED")
        time.sleep(2)

