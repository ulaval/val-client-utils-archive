"""
Ce module définit l'interface cli BOV (Boîte à Outils VALERIA)
"""
import os
import json
import getpass

from plumbum import cli, colors
from keycloak import KeycloakOpenID
from val_client_utils import __version__
from val_client_utils.val_services.dataset import FavoriteDataset
from val_client_utils.download import slurm_dl


class BOV(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Boîte à Outils VALERIA (BOV). Ce outil en ligne de commande encapsule les fonctionnalités du " \
                  "paquet python val_client_utils."
    COLOR_MANDATORY = colors.bold & colors.yellow

    def __init__(self, *args):
        super().__init__(*args)

    def main(self, *args):
        pass


@BOV.subcommand("token")
class BOVVALToken(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Obtenir un token oauth pour interroger val-services.\n" \
                  "Les variables d'environnement KC_CLIENT, KC_SECRET, " \
                  "KC_HOST doivent exister."

    def main(self, *args):
        needed_env = {"KC_CLIENT", "KC_SECRET", "KC_HOST"}
        if not needed_env.issubset(set(os.environ.keys())):
            raise Exception(f"Les variables d'environnement {needed_env} sont requises.")
        user = input("Username:")
        password = getpass.getpass("Password:")
        kc = KeycloakOpenID(server_url=os.environ['KC_HOST'],
                            client_id=os.environ['KC_CLIENT'],
                            realm_name="valeria",
                            client_secret_key=os.environ['KC_SECRET'])
        authentication = kc.token(user, password)
        print(f"export VAL_OAUTH_TOKEN={authentication['access_token']}")
        return 0


@BOV.subcommand("favoris")
class BOVFavoris(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Obtenir les jeux de données identifiés comme favoris à partir du portail VALERIA"

    ids = cli.SwitchAttr(["-i", "--id"], str, default=None,
                         help="Pour de sélection un ou plusieurs ID de jeu de données "
                              "à extraire. Il s'agit d'une liste séparée par des "
                              "virgules.")

    resources_ids = cli.SwitchAttr(["-j", "--rid"], str, default=None,
                                   help="Pour de sélection un ou plusieurs ID de ressources "
                                        "à extraire. Il s'agit d'une liste séparée par des "
                                        "virgules.")

    resources_format = cli.SwitchAttr(["-f", "--rformat"], str, default=None,
                                      help="Filtrer les ressources à partir des format de fichiers. Il s'agit d'une "
                                           "liste séparée par des virgules.")

    list = cli.Flag(["-l", "--lister"], help="Lister les favoris")
    list_resources = cli.Flag(["-r", "--lister-ressources"], help="Lister les ressources")
    list_resources_url = cli.Flag(["-u", "--lister-les-url"], help="Lister l'URL de chaque ressource")
    no_prefix = cli.Flag(["-n", "--sans-prefixe"], help="Ne pas afficher le type et l'id")

    def main(self, *args):
        if "VAL_OAUTH_TOKEN" not in os.environ.keys():
            raise Exception("La variable d'environnement VAL_OAUTH_TOKEN doit exister")
        fd = FavoriteDataset(os.environ["VAL_OAUTH_TOKEN"])
        fav_data = fd.get_dataset()

        if self.ids:
            filtered_fav = []
            for fav in fav_data:
                if fav['id'] in self.ids.split(','):
                    filtered_fav += [fav]
                for r in fav["resources"]:
                    if r['id'] in self.ids.split(','):
                        filtered_fav += [fav]
            fav_data = filtered_fav

        if self.list or self.list_resources or self.list_resources_url:
            for fav in fav_data:
                if self.list:
                    prefix = "" if self.no_prefix else "title", fav["id"]
                    print(*prefix, fav["title"])

                if self.list_resources or self.list_resources_url:
                    for r in fav["resources"]:
                        if not self.resources_ids or r["id"] in self.resources_ids.split(","):
                            if not self.resources_format or r["format"] in self.resources_format.split(","):
                                url = r['url'] if self.list_resources_url else ""
                                name = r['name'].replace(" ", "_") if self.list_resources else ""
                                prefix = "" if self.no_prefix else ["resources", r["id"]]
                                print(*prefix, url, name)

        else:
            print(json.dumps(fav_data))
        return 0


@BOV.subcommand("download")
class BOVDownload(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Permet de télécharger des fichiers à partir du noeud externe."

    def main(self, *urls):
        slurm_dl.wget(urls)


if __name__ == "__main__":
    BOV.run()
