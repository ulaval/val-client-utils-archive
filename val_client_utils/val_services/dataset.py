"""
Ce package contient les méthodes qui interrogent le service val-services ServiceRestJeuxDonnees
Voir l'implémentation: https://bitbucket.org/ulaval/val-services/src/develop/val-apis/src/main/java/ca/ulaval/val/apis/jeuxdonnees/ServiceRestJeuxDonnees.java
"""
import requests


class FavoriteDataset:
    def __init__(self, access_token: str):
        self.access_token = access_token

    def get_dataset(self):
        url = 'https://services.valeria.science/val/v1/jeuxdonnees/favoris'
        header = {'Accept': 'application/json',
                  'Authorization': 'Bearer ' + self.access_token,
                  'Content-Type': 'application/json'}
        r = requests.get(url, headers=header)
        r.raise_for_status()
        return r.json()
