import json
from val_client_utils import BOV, BOVFavoris
from test.helpers import MockJDDGetFavorites
from val_client_utils.val_services.dataset import FavoriteDataset


class TestBOV:

    def test_bov_main(self, capsys):
        _, rc = BOV.run(["bov", "-h"], exit=False)
        assert rc == 0
        stdout, stderr = capsys.readouterr()
        assert "Boîte à Outils VALERIA" in stdout

    def test_bov_favoris(self, monkeypatch, capsys):
        def mock_get(*args, **kwargs):
            return MockJDDGetFavorites.get_dataset()

        monkeypatch.setattr(FavoriteDataset, "get_dataset", mock_get)
        monkeypatch.setenv("VAL_OAUTH_TOKEN", "fakevalue", prepend=False)

        _, rc = BOVFavoris.run(["bov", "favoris"], exit=False)
        assert rc == 0
        stdout, stderr = capsys.readouterr()
        fav = json.loads(stdout)
        assert fav[0]["id"] == "30bafcfe-b63e-4af4-a179-e64ed606a403"

    def test_bov_favoris_lister(self, monkeypatch, capsys):
        def mock_get(*args, **kwargs):
            return MockJDDGetFavorites.get_dataset()

        monkeypatch.setattr(FavoriteDataset, "get_dataset", mock_get)
        monkeypatch.setenv("VAL_OAUTH_TOKEN", "fakevalue", prepend=False)

        _, rc = BOVFavoris.run(["bov", "favoris", "-l"], exit=False)
        assert rc == 0
        stdout, stderr = capsys.readouterr()
        assert "30bafcfe-b63e-4af4-a179-e64ed606a403" in stdout

    def test_bov_favoris_resources(self, monkeypatch, capsys):
        def mock_get(*args, **kwargs):
            return MockJDDGetFavorites.get_dataset()

        monkeypatch.setattr(FavoriteDataset, "get_dataset", mock_get)
        monkeypatch.setenv("VAL_OAUTH_TOKEN", "fakevalue", prepend=False)

        _, rc = BOVFavoris.run(["bov", "favoris", "-r"], exit=False)
        assert rc == 0
        stdout, stderr = capsys.readouterr()
        assert "26fe1511-f2d9-4d4e-834e-18f5cdfe860d" in stdout
