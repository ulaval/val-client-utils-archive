import os
import pytest
from test.helpers import get_token
from val_client_utils.val_services.dataset import FavoriteDataset


@pytest.mark.skipif(not {"KC_CLIENT", "KC_SECRET", "KC_HOST", "KC_USER", "KC_PASS"}.issubset(set(os.environ.keys())),
                    reason="Il s'agit d'un test qui nécessite un jeton valide et un val-services disponible.")
class TestJeuDeDonnees:

    def test_jeu_de_donnees_token_invalide(self, get_token):
        ds = FavoriteDataset("faketoken")
        with pytest.raises(Exception):
            ds.get_dataset()

    def test_jeu_de_donnees_token_valide(self, get_token):
        ds = FavoriteDataset(get_token['access_token'])
        ds.get_dataset()
