import os
import json
import pytest
from keycloak import KeycloakOpenID

pjoin = os.path.join
current_dir = os.path.abspath(os.path.dirname(__file__))

@pytest.fixture
def get_token():
    """
    Retourne un dictionnaire avec jeton oauth au format suivant:
    {
     'access_token': 'tokenblabla',
     'expires_in': 14400,
     'not-before-policy': 1563211445,
     'refresh_expires_in': 1800,
     'refresh_token': 'tokenblabla',
     'scope': 'profile email',
     'session_state': '12404910-a8b0-47a9-9b46-6f4d90272fe8',
     'token_type': 'bearer'
    }
    """
    kc = KeycloakOpenID(server_url=os.environ['KC_HOST'],
                        client_id=os.environ['KC_CLIENT'],
                        realm_name="valeria",
                        client_secret_key=os.environ['KC_SECRET'])
    authentication = kc.token(os.environ["KC_USER"], os.environ["KC_PASS"])
    return authentication


class MockJDDGetFavorites:
    @staticmethod
    def get_dataset():
        fav = {}
        with open(f"{current_dir}/samples/favoris.json") as fp:
            fav = json.load(fp)
        return fav
